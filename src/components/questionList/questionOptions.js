import React, { PropTypes } from 'react'

import TextField from 'material-ui/TextField'
import Checkbox from 'material-ui/Checkbox'
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';


const QuestionOptions = ({ options }) => {
     switch(options.type) {
      case 'value': return (
            <TextField
          floatingLabelText="Введите числовое значение"
          type="number"
        />
      )
      case 'text' :
        return (<TextField
        hintText="Текст"
        multiLine={true}
        rows={1}
        rowsMax={4}
      />)
      case 'selone' :
        return (
          <RadioButtonGroup name={'selone' + options.qid} defaultSelected="1">
          {options.answers.map((answer, index) =>
            <RadioButton key={options.qid + index}
              value={index}
              label={answer}
            />
                )}
    </RadioButtonGroup>
        )
      case 'selmulti' : return (
        <div>
        {options.answers.map((answer, index) =>
          <Checkbox key={options.qid + index}
            value={index}
            label={answer}
          />
              )}
              </div>
      )
      default: console.log('in default'); return ''

     }
  }

QuestionOptions.propTypes = {
  options : PropTypes.object.isRequired
}

export default QuestionOptions



/*
QuestionOptions.propTypes = {
  options : PropTypes.objectOf(PropTypes.shape({
    qid: PropTypes.number.isRequired,
    type: PropTypes.string.isRequired,
    answers: PropTypes.arrayOf(React.PropTypes.string).isRequired
  }).isRequired).isRequired
}

*/
/*
options : PropTypes.arrayOf(React.PropTypes.objectOf(PropTypes.shape({
  qid: PropTypes.number.isRequired,
  type: PropTypes.string.isRequired,
  answers: PropTypes.arrayOf(React.PropTypes.number).isRequired
}).isRequired).isRequired).isRequired
{options.answers.map((answer, index) =>
  {<Checkbox key={options.qid + index}
    label={answer}
  />}
      )}
*/
