import React, { PropTypes } from 'react'

import TextField from 'material-ui/TextField'
import {Card, CardHeader, CardText} from 'material-ui/Card';
import QuestionOptions from './questionOptions'


import {SortableContainer, SortableElement, SortableHandle, arrayMove} from 'react-sortable-hoc';

const findOptions = function(options, id) {
  for (var i = 0, len = options.length; i < len; i++) {
    if (options[i].qid === id) {
      return options[i]
    }
  }
}

const questionsMove = function(arr, oldIndex, newIndex) {
    if (oldIndex == newIndex) {
      console.log("yeeees");
      if (oldIndex + 1 < arr.length && arr[oldIndex + 1].type === 'lq') {
        arr[oldIndex].level ++;
        var temp = arr[oldIndex];
        arr[oldIndex] = arr[oldIndex + 1];
        arr[oldIndex + 1] = temp;
        return arr;
      }
    }
      if (arr[oldIndex].type === 'lq') {
        console.log("move lq");
        var movedTo = oldIndex
        while (movedTo < arr.length - 1) {
          if (arr[movedTo + 1].level > arr[oldIndex].level) {
            ++movedTo
          } else {
            break
          }
        }
        var tempIndex = movedTo;
        while (movedTo >= oldIndex) {
          if (oldIndex <= newIndex) {
            arr = arrayMove(arr, oldIndex, newIndex)
          } else {
            arr = arrayMove(arr, tempIndex, newIndex)
          }
          --movedTo
        }
        return arr
      }
    if (arr[newIndex].type === 'lq') {
      console.log("move to lq");
      if (oldIndex <= newIndex) {
        var movedTo = newIndex
        while (movedTo < arr.length - 1) {
          if (arr[movedTo + 1].level > arr[newIndex].level) {
            ++movedTo
          } else {
            break
          }
        }
        return arrayMove(arr, oldIndex, movedTo)
      } else {
        return arrayMove(arr, oldIndex, newIndex)
      }
    }
    if (arr[newIndex].level != arr[oldIndex].level) {
      var level = arr[newIndex].level
      arr = arrayMove(arr, oldIndex, newIndex)
      arr[newIndex].level = level
      return arr;
    }
    var arr = arrayMove(arr, oldIndex, newIndex)
    return arr
}

const QuestionList = ({ questions, options, changeQuestionsOrder, level }) => {
    //Sorted part -------------------------------------
    var DragHandle = SortableHandle((e) => {
      var qOptions = findOptions(options, e.value.id)
      if (e.value.level === level && e.value.type === 'q') {
        return(<Card>
        <CardHeader
          title={e.value.name}
          actAsExpander={true}
          showExpandableButton={true}
        />
        <CardText expandable={true}>
          <QuestionOptions options = {qOptions}/>
        </CardText>
      </Card>)
    } else if (e.value.level === level && e.value.type === 'lq') {
      var index = questions.findIndex(x => x.id==e.value.id);
      return(<Card>
      <CardHeader
        title={"Подраздел:" + e.value.name}
        actAsExpander={true}
        showExpandableButton={true}
      />
      <CardText expandable={true}>
        <QuestionList questions={questions.slice(index+1)} options={options}
                                changeQuestionsOrder={changeQuestionsOrder}
                                level = {e.value.level + 1}/>
      </CardText>
    </Card>)
      } else if (e.value.level < level) {
        console.log("retun this");
        return (<div></div>)
      } else {
        return(<div></div>)
      }
    }
    );
    var SortableItem = SortableElement(({value}) => {
      return (
          <li>
              <DragHandle value={value}/>
          </li>
      )
  });
    var SortableList = SortableContainer(({items}) => {
      return (
        <ul style={{listStyleType: 'none'}}>
           {items.map((value, index) =>
                     <SortableItem key={`item-${index}`} index={index} value={value} />
                 )}
        </ul>
      );
    });
    var onSortEnd = ({oldIndex, newIndex}) => { changeQuestionsOrder(questionsMove(questions, oldIndex, newIndex))}
    //--------------------------------------------------
    return (
      <SortableList items={questions} onSortEnd={onSortEnd} useDragHandle={true} />
    );
  }

QuestionList.propTypes = {
  questions : PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
    level: PropTypes.number.isRequired,
  }).isRequired).isRequired,
  options : PropTypes.arrayOf(PropTypes.shape({
    qid: PropTypes.number.isRequired,
    type: PropTypes.string.isRequired,
    answers: PropTypes.arrayOf(React.PropTypes.string).isRequired
  }).isRequired).isRequired
}

export default QuestionList







/*
for (var index = 0; index < questions.length; index++) {
var qOptions = findOptions(options, questions[index].id)
if (questions[index].level === level && questions[index].type === 'q') {
  qComponents.push(<Card>
  <CardHeader
    title={questions[index].name}
    actAsExpander={true}
    showExpandableButton={true}
  />
  <CardText expandable={true}>
    <QuestionOptions options = {qOptions}/>
  </CardText>
</Card>)
} else if (questions[index].level === level && questions[index].type === 'ql') {
qComponents.push(<Card>
<CardHeader
  title={questions[index].name}
  actAsExpander={true}
  showExpandableButton={true}
/>
<CardText expandable={true}>
  <QuestionList questions={questions.slice(index+1)} options={options}/>
</CardText>
</Card>)
} else if (questions[index].level < level) {
  break
}
}
*/
