//диалог открывающийся после нажатия на кнопку жобавить

import React, { PropTypes } from 'react'
import { connect } from 'react-redux'

import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import Checkbox from 'material-ui/Checkbox';
import TextField from 'material-ui/TextField';

import QDetails from './dialogdetails/questiondetails'
import TDetails from './dialogdetails/templatedetails'

import {ContentState} from 'draft-js'
import {changeNewNameInDialog} from '../actions/index'
import * as constants from '../constants/listvaluetypes'



class DialogToCreate extends React.Component {
  state = {
    open: false,
    isChecked : false,
    question : {type: 'q', opttype:'value', options:[]}
  };

  handleOpen = () => {
    this.setState({open: true})
  }

  handleClose = () => {
    this.setState({open: false})
  }

  handleSubmitClose = (e) => {
    this.handleClose()
    this.setState({isChecked : false})
    switch (this.props.type) {
      case constants.DEPARTMENT_LIST:
        return this.props.onAddClick(this.props.name)
      case constants.BLOCK_LIST:
        return this.props.onAddClick(this.props.name, this.state.isChecked)
      case constants.SUBBLOCK_LIST:
        if (this.props.blockType === constants.BLOCK_TYPE_1) {
          var q = this.state.question
          q.name = this.props.name
          return this.props.onAddClick(q)
        } else {
          var properties = this.child.getProperties();
          var hint = properties.substring(properties.indexOf("; hint: ") + 8);
          var block = ContentState.createFromText(
            properties.substring(0, properties.indexOf("; hint: ")));
          return this.props.onAddClick(block, hint);

        }
    }
  }

  getQuestionDetails(type, opttype, options) {
    this.setState( {question : {type: type, opttype:opttype, options:options}})
  }

  getTemplateDetails() {

  }

//this method for changing of the name of department;
  handleChange= (e) => {
    this.props.changeNewNameInDialog(e.target.value)
  }

  handleOnCheckBoxCheck = (e) => {
      this.setState({isChecked : e.target.value === 'on'})
  }

  render() {
    const actions = [
      <FlatButton
        label="Отменить"
        primary={true}
        onTouchTap={this.handleClose}
      />,
      <FlatButton
        label="Создать"
        primary={true}
        onTouchTap={this.handleSubmitClose}
      />,
    ];

    return (
      <div>
        <RaisedButton label="Добавить" onTouchTap={this.handleOpen} primary={true}/>
        <Dialog
          title={this.props.title}
          actions={actions}
          modal={true}
          open={this.state.open}
          autoScrollBodyContent={true}
        >
        { this.props.type === constants.SUBBLOCK_LIST &&
          this. props.blockType === constants.BLOCK_TYPE_1 ||
          this.props.type === constants.BLOCK_LIST || this.props.type === constants.DEPARTMENT_LIST ?
        (
          <TextField
          defaultValue= {this.props.name}
          floatingLabelText={this.props.label}
          fullWidth= {true}
          onChange={this.handleChange}
        />
      )
      : ''
      }
        {
          this.props.type === constants.BLOCK_LIST ?
          (<Checkbox label="Шаблон документа" onCheck={this.handleOnCheckBoxCheck.bind(this)}/>) :
          this.props.type === constants.SUBBLOCK_LIST && this.props.blockType === constants.BLOCK_TYPE_1 ?
          (<QDetails callBack = {this.getQuestionDetails.bind(this)} />) :
          this.props.type === constants.SUBBLOCK_LIST && this.props.blockType === constants.BLOCK_TYPE_2 ?
          (<TDetails ref={instance => { this.child = instance; }} />) : ''
        }
        </Dialog>
      </div>
    );
  }
}

DialogToCreate.propTypes = {
  title : PropTypes.string.isRequired,
  label : PropTypes.string.isRequired,
  onAddClick: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  blockType : PropTypes.string.isRequired
}

export default connect(null, {changeNewNameInDialog})(DialogToCreate)
