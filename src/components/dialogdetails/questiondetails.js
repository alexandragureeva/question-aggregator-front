import React, { PropTypes } from 'react'
 import { Grid, Flex } from 'reflexbox'
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentRemove from 'material-ui/svg-icons/content/remove';

class QDetails extends React.Component {
    constructor(props) {
      super(props)
      this.handleRBChange = this.handleRBChange.bind(this)
      this.addOption = this.addOption.bind(this)
      this.deleteOption = this.deleteOption.bind(this)
      this.handlePageTypeChange = this.handlePageTypeChange.bind(this)
      this.changeOption = this.changeOption.bind(this)
    }
    state = {
      numChildren: 0,
      qType : 'q',
      optionType : 'value',
      showOptionTypes: true,
      showAddButton : false,
      options : []
    }


    handlePageTypeChange(e) {
        if (e.target.value === 'lq') {
          this.setState({showOptionTypes: false, showAddButton: false,
             numChildren : 0, options : [], qType : 'lq', optionType : ''})
             this.props.callBack('lq', '', [])
        } else {
          this.setState({showOptionTypes: true, qType : 'q'})
          this.props.callBack('q', this.state.optionType, this.state.options)
        }
    }

    handleRBChange(e) {
        switch(e.target.value) {
          case ('value'): this.setState({showAddButton : false,
            numChildren :0, options : [], optionType: 'value'});
            this.props.callBack(this.state.qType, 'value', []);
              break
          case ('text'): this.setState({showAddButton : false,
            numChildren :0, options : [], optionType: 'text'});
            this.props.callBack(this.state.qType, 'text', []);
              break;
          case ('selone'): this.setState({showAddButton : true, optionType: 'selone'});
          this.props.callBack(this.state.qType, 'selone', this.state.options);
            break;
          case ('selmulti'): this.setState({showAddButton : true, optionType: 'selmulti'});
          this.props.callBack(this.state.qType, 'selmulti', this.state.options);
            break;
        }
    }

    addOption() {
      var newOptions = this.state.options.concat('');
      this.setState({numChildren : this.state.numChildren + 1, options: newOptions})
      this.props.callBack(this.state.qType,
        this.state.optionType, newOptions)
    }


    deleteOption(e) {
      var tempOptions = this.state.options
      tempOptions.splice(e, 1)
      this.setState({options : tempOptions, numChildren : this.state.numChildren - 1})
      this.props.callBack(this.state.qType,
        this.state.optionType, tempOptions)
    }

    changeOption(e) {
      var tempOptions = this.state.options
      tempOptions[+e.target.dataset.id] = e.target.value
      this.setState({options : tempOptions})
      this.props.callBack(this.state.qType,
        this.state.optionType, tempOptions)
    }

    render() {
      const children = [];
        for (var i = 0; i < this.state.numChildren; i ++) {
            children.push(
              <div key={i}>
              <TextField hintText={"Значение " + (i+1)} value = {this.state.options[i]} onChange={(e)=>this.changeOption(e)} data-id = {i} />
              <FloatingActionButton mini={true} value={i} onClick={this.deleteOption.bind(null, i)} key={i}>
               <ContentRemove />
             </FloatingActionButton>
              </div>
            )
        }
      return (
        <div>
        <RadioButtonGroup name="shipSpeed" defaultSelected="q" onChange = {this.handlePageTypeChange}>
          <RadioButton
            value="q"
            label="Вопрос"
          /><RadioButton
            value="lq"
            label="Список вопросов"
          />
          </RadioButtonGroup>
          { this.state.showOptionTypes ?
          (<div >
            <RadioButtonGroup name="shipSpeed" defaultSelected="value" onChange = {this.handleRBChange}>
              <RadioButton
                value="value"
                label="Числовое значение"
              />
              <RadioButton
                value="text"
                label="Текст"
              />
              <RadioButton
                value="selone"
                label="Выбор из списка"
              />
              <RadioButton
                value="selmulti"
                label="Множественный выбор из списка"
              />
              </RadioButtonGroup>
              <div className="options">
                {children}
              </div>
              {this.state.showAddButton ?
                (<RaisedButton label="Добавить опцию" primary={true} onClick={this.addOption}/>)
                : (<RaisedButton label="Добавить опцию" disabled = {true}/>)}
          </div>) : '' }
        </div>
        );
      }
}

QDetails.propTypes = {
  callBack : PropTypes.func.isRequired
}

export default QDetails
