import React, { PropTypes } from 'react'
 import { Grid, Flex } from 'reflexbox'
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentRemove from 'material-ui/svg-icons/content/remove';

class TDetails extends React.Component {
    constructor(props) {
      super(props)

    }
    state = {

    }

    getProperties() {
      return this.block.getValue() + "; hint: " + this.hint.getValue();
    }


    render() {
      return (
        <div>
        <TextField
          floatingLabelText={'Новый блок документа'}
          fullWidth= {true}
          multiLine={true}
          rows = {10}
          ref={(block) => this.block = block}
        />
        <TextField
          floatingLabelText={'Подсказка для данного блока'}
          fullWidth= {true}
          multiLine={true}
          rows = {3}
          ref={(hint) => this.hint = hint}
        />
        </div>
        );
      }
}


export default TDetails
