import React, { PropTypes } from 'react'

import IconButton from 'material-ui/IconButton'
import TextField from 'material-ui/TextField'
import Checkbox from 'material-ui/Checkbox'
import {grey400, darkBlack, lightBlack} from 'material-ui/styles/colors';
import Edit from 'material-ui/svg-icons/image/edit';
import {ListItem} from 'material-ui/List'



const Block = ({ onClick, Name, Key, Id, onEditClick, edit, onChange }) => {
  var onEdit = function(e) {
    console.log(e);
    e.stopPropagation()
    e.nativeEvent.stopImmediatePropagation();
    onEditClick()
  }

  var EditElement = (
    <IconButton
      touch={true}
      tooltip="edit"
      tooltipPosition="bottom-left"
      onClick={onEdit}
    >
      <Edit color={grey400} hoverColor={lightBlack}/>
    </IconButton>
  )
  var  handleEditName = function(name, Id, type){
      onChange(name, Id, type);
    }

    var block =''
    if (!edit ) {
      if (Key === 'null')
        block = <ListItem primaryText={Name} leftCheckbox={<Checkbox onClick={onClick}/>}   rightIconButton={EditElement}/>
      else
        block = <ListItem primaryText={Name} secondaryText={"Department"} leftCheckbox={<Checkbox onClick={onClick}/>}  rightIconButton={EditElement}/>
  } else {
    if (Key === 'null')
      block = <ListItem primaryText={[<TextField key={0} name={Name} defaultValue={Name} onChange={(e) => handleEditName(e.target.value, Id,  "BLOCK_LIST")}/>]}
      leftCheckbox={<Checkbox onClick={onClick}/>}  rightIconButton={EditElement}/>
    else
      block = <ListItem primaryText={[<TextField key={0} name={Name} defaultValue={Name} onChange={(e) => handleEditName(e.target.value, Id,  "DEPARTMENT_LIST")}/>]}
      secondaryText={"Department"} leftCheckbox={<Checkbox onClick={onClick}/>}  rightIconButton={EditElement}/>
  }
  return block;
  }

Block.propTypes = {
  onClick: PropTypes.func.isRequired,
  Name: PropTypes.string.isRequired,
  Key: PropTypes.string.isRequired,
  Id: PropTypes.number.isRequired,
  edit : PropTypes.bool.isRequired
}

export default Block
