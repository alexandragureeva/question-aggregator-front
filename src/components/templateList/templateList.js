import React, { PropTypes } from 'react'
import { connect } from 'react-redux'
import { Grid, Flex } from 'reflexbox'

import TextField from 'material-ui/TextField'
import {Card, CardHeader, CardText} from 'material-ui/Card'

import {SortableContainer, SortableElement, SortableHandle, arrayMove} from 'react-sortable-hoc'
import TBlockEditor from './tBlockEditor'
import RaisedButton from 'material-ui/RaisedButton';

import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';

const style = {
  margin: 12,
};

class TemplateList extends React.Component{

  constructor(prop) {
    super(prop)
    this.state = {value: ''};
    this.saveStates.bind(this);
  }

  handleChange = (event, index, value) => this.setState({value});


  saveStates(tBlocks, setNewState) {
  for (var i = 0; i < tBlocks.length; i++) {
    setNewState(tBlocks[i].id, this[`editor${tBlocks[i].id}`].getState());
  }
  }



  insertValue(tBlocks, value) {
    for (var i = 0; i < tBlocks.length; i++) {
      this[`editor${tBlocks[i].id}`].insertValue(value);
    }
  }
  render() {
        //Sorted part -------------------------------------
    var DragHandle = SortableHandle((e) => {
        return(
          <Flex col={12} p={2} key = {e.value.id}>
              <Grid col={8}>
                <TBlockEditor tblock = {e.value.text} onRef={ref => (this[`editor${e.value.id}`] = ref)}/>
              </Grid>
              <Grid col={4}>
              {e.value.hint}
              </Grid>
              </Flex>
          );
    }
    );
    var SortableItem = SortableElement(({value}) => { return (<li><DragHandle value={value}/></li>);});
    var SortableList = SortableContainer(({items}) => {
      return (
        <ul style={{listStyleType: 'none'}}>
           {items.map((value, index) =>
                     <SortableItem key={`item-${index}`} index={index} value={value} />
                 )}
        </ul>
      );
    });

    var onSortEnd = ({oldIndex, newIndex}) => { this.props.changeTBlocksOrder(arrayMove(this.props.tBlocks, oldIndex, newIndex))}
    //--------------------------------------------------
    const items = [];
    items.push(<MenuItem value={''} key={-1} primaryText={"Выберите поле"} />)
    for (let i = 0; i < this.props.questionnaires.length; i++ ) {
      items.push(<MenuItem value={this.props.questionnaires[i].uName} key={i} primaryText={this.props.questionnaires[i].name} />);
    }
    return (
      <div>
      <DropDownMenu maxHeight={300} value={this.state.value} onChange={this.handleChange}>
              {items}
      </DropDownMenu>
      <RaisedButton label="Вставить поле"  primary={true} style={style} onClick={() => this.insertValue(this.props.tBlocks, this.state.value)}/>
      <SortableList items={this.props.tBlocks} onSortEnd={onSortEnd} pressDelay={400} />
      <RaisedButton label="Сохранить"  primary={true} style={style} onClick={() => this.saveStates(this.props.tBlocks, this.props.setNewState)}/>
      </div>
    );
  }
  }

TemplateList.propTypes = {
  tBlocks : PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number.isRequired,
    text: PropTypes.object.isRequired,
    hint: PropTypes.string.isRequired
  }).isRequired).isRequired,
  questionnaires : PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string.isRequired,
    uName: PropTypes.string.isRequired
  }).isRequired).isRequired,
  setNewState : PropTypes.func.isRequired,
  changeTBlocksOrder : PropTypes.func.isRequired
}

export default connect(null, {})(TemplateList)
