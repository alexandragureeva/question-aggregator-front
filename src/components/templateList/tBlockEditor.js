import React, { PropTypes } from 'react'
import ReactDOM from 'react-dom';
import { connect } from 'react-redux'
import {Editor, EditorState, RichUtils, ContentState, Modifier, DefaultDraftBlockRenderMap } from 'draft-js'
import { OrderedSet, Map } from 'immutable'
import styles from '../../../css/editor.css';

     export default class TBlockEditor extends React.Component {
       constructor(props) {
         super(props);
         this.state = {editorState: EditorState.createWithContent(this.props.tblock), value : '', readyToSet : 0, readyFunc : ''};

         this.focus = () => this.refs.editor.focus();
         this.onChange = (editorState) => this._onChange(editorState);

         this.handleKeyCommand = (command) => this._handleKeyCommand(command);
         this.onTab = (e) => this._onTab(e);
         this.toggleBlockType = (type) => this._toggleBlockType(type);
         this.toggleInlineStyle = (style) => this._toggleInlineStyle(style);

         const blockRenderMap = Map({
            'text-align-left': {
                element: 'div'
            },
            'text-align-center': {
                element: 'div'
            },
            'text-align-right': {
                element: 'div'
            }
        });
        this.extendedBlockRenderMap = DefaultDraftBlockRenderMap.merge(blockRenderMap);
       }

       componentDidMount() {
          this.props.onRef(this)
        }
        componentWillUnmount() {
          this.props.onRef(undefined)
        }

        _onChange(editorState) {
            var selectionState = editorState.getSelection();
            var currentContent = editorState.getCurrentContent();
            var anchorKey = selectionState.getAnchorKey();
            var currentContentBlock = currentContent.getBlockForKey(anchorKey);
            var start = selectionState.getStartOffset();
            var end = selectionState.getEndOffset();
            var selectedText = currentContentBlock.getText().slice(start, end);
            console.log(start);
            if (this.state.readyToSet === 1) {
              console.log(start + " in insert");
              var insertedConState = Modifier.insertText(currentContent, selectionState, " " + this.state.value + " ", OrderedSet.of('INSER'));
              const es = EditorState.push(editorState, insertedConState, 'insert-fragment');
              this.setState({editorState : es, readyToSet : 0})
            }
            else {
                this.setState({editorState: editorState, readyToSet : --this.state.readyToSet});
            }
          }

        insertValue(value) {
          this.setState({readyToSet : 2, value : value})
        }

       _handleKeyCommand(command) {
         const {editorState} = this.state;
         const newState = RichUtils.handleKeyCommand(editorState, command);
         if (newState) {
           this.onChange(newState);
           return true;
         }
         return false;
       }

       getState() {
          return this.state.editorState.getCurrentContent();
       }

       _onTab(e) {
         const maxDepth = 4;
         this.onChange(RichUtils.onTab(e, this.state.editorState, maxDepth));
       }

       _toggleBlockType(blockType) {
         this.onChange(
           RichUtils.toggleBlockType(
             this.state.editorState,
             blockType
           )
         );
       }

       _toggleInlineStyle(inlineStyle) {
         this.onChange(
           RichUtils.toggleInlineStyle(
             this.state.editorState,
             inlineStyle
           )
         );
       }

       render() {
         const {editorState} = this.state;

         // If the user changes block type before entering any text, we can
         // either style the placeholder or hide it. Let's just hide it now.
         let className = 'RichEditor-editor';
         var contentState = editorState.getCurrentContent();
         if (!contentState.hasText()) {
           if (contentState.getBlockMap().first().getType() !== 'unstyled') {
             className += ' RichEditor-hidePlaceholder';
           }
         }

         return (
           <div className="RichEditor-root">
             <BlockStyleControls
               editorState={editorState}
               onToggle={this.toggleBlockType}
             />
             <InlineStyleControls
               editorState={editorState}
               onToggle={this.toggleInlineStyle}
             />
             <div className={className} onClick={this.focus}>
               <Editor
                 blockRenderMap={this.extendedBlockRenderMap}
                 blockStyleFn={getBlockStyle}
                 customStyleMap={styleMap}
                 editorState={editorState}
                 handleKeyCommand={this.handleKeyCommand}
                 onChange={this.onChange}
                 onTab={this.onTab}
                 placeholder=""
                 ref="editor"
                 spellCheck={true}
               />
             </div>
           </div>
         );
       }
     }

     // Custom overrides for "code" style.
     const styleMap = {
       CODE: {
         backgroundColor: 'rgba(0, 0, 0, 0.05)',
         fontFamily: '"Inconsolata", "Menlo", "Consolas", monospace',
         fontSize: 16,
         padding: 2,
       },
       INSER : {
         backgroundColor: 'rgba(170,170,170,0.8)',
         fontWeight : 'bold',
         color : 'red'
       }
     };

     function getBlockStyle(block) {
       switch (block.getType()) {
         case 'text-align-left': return 'RichEditor-left';
         case 'text-align-center':  return 'RichEditor-center';
         case 'text-align-right': return 'RichEditor-right';
         default: return null;
       }
     }

     class StyleButton extends React.Component {
       constructor() {
         super();
         this.onToggle = (e) => {
           e.preventDefault();
           this.props.onToggle(this.props.style);
         };
       }

       render() {
         let className = 'RichEditor-styleButton';
         if (this.props.active) {
           className += ' RichEditor-activeButton';
         }

         return (
           <span className={className} onMouseDown={this.onToggle}>
             {this.props.label}
           </span>
         );
       }
     }

     const BLOCK_TYPES = [
       {label: 'H1', style: 'header-one'},
       {label: 'H2', style: 'header-two'},
       {label: 'H3', style: 'header-three'},
       {label: 'H4', style: 'header-four'},
       {label: 'H5', style: 'header-five'},
       {label: 'H6', style: 'header-six'},
       {label: 'UL', style: 'unordered-list-item'},
       {label: 'OL', style: 'ordered-list-item'},
       {label: 'Left', style: 'text-align-left'},
       {label: 'Right', style: 'text-align-right'},
       {label: 'Center', style: 'text-align-center'},
     ];

     const BlockStyleControls = (props) => {
       const {editorState} = props;
       const selection = editorState.getSelection();
       const blockType = editorState
         .getCurrentContent()
         .getBlockForKey(selection.getStartKey())
         .getType();

       return (
         <div className="RichEditor-controls">
           {BLOCK_TYPES.map((type) =>
             <StyleButton
               key={type.label}
               active={type.style === blockType}
               label={type.label}
               onToggle={props.onToggle}
               style={type.style}
             />
           )}
         </div>
       );
     };

     var INLINE_STYLES = [
       {label: 'Bold', style: 'BOLD'},
       {label: 'Italic', style: 'ITALIC'},
       {label: 'Underline', style: 'UNDERLINE'},
       {label: 'Monospace', style: 'CODE'},
     ];

     const InlineStyleControls = (props) => {
       var currentStyle = props.editorState.getCurrentInlineStyle();
       return (
         <div className="RichEditor-controls">
           {INLINE_STYLES.map(type =>
             <StyleButton
               key={type.label}
               active={currentStyle.has(type.style)}
               label={type.label}
               onToggle={props.onToggle}
               style={type.style}
             />
           )}
         </div>
       );
     };

     TBlockEditor.propTypes = {
       tblock : PropTypes.object.isRequired
     }
