import React, { PropTypes } from 'react'
import ValuesList from '../containers/valueslist'
import DialogToCreate from './dialogtocreate'
 import { Grid, Flex } from 'reflexbox'
import RaisedButton from 'material-ui/RaisedButton';

const MainPage = ({ name, onAddClick, title, label, nameForDialog, type, blockType }) => (
<div>
<Flex col={12} p={2}>
    <Grid col={9}>
      <h2>{ name }</h2>
    </Grid>
    <Grid auto>
      <DialogToCreate title={title} label={label} onAddClick={onAddClick}
          name = {nameForDialog} type = {type} blockType = {blockType}/>
    </Grid>
    </Flex>
      <ValuesList/>
</div>
)


MainPage.propTypes = {
  name : PropTypes.string.isRequired,
  label : PropTypes.string.isRequired,
  title : PropTypes.string.isRequired,
  nameForDialog : PropTypes.string.isRequired,
  onAddClick: PropTypes.func.isRequired,
  type: PropTypes.string.isRequired
}

export default MainPage
