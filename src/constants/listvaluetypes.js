export const DEPARTMENT_LIST = 'DEPARTMENT_LIST'
export const BLOCK_LIST = 'BLOCK_LIST'
export const SUBBLOCK_LIST = 'SUBBLOCK_LIST'

export const DEPARTMENT_NAME = 'Отделения больницы'
export const BLOCK_NAME = 'Блоки для отделения '

export const BLOCK_TYPE_1 = 'Question list'
export const BLOCK_TYPE_2 = 'Doc creator'
