/*
this file contains actions for departments case
*/
import fetch from 'isomorphic-fetch'

import * as general from './index'

export const REQUEST_DEPARTMENTS = 'REQUEST_DEPARTMENTS'
export const RECEIVE_DEPARTMENTS = 'RECEIVE_DEPARTMENTS'
export const RECEIVE_ADDDEPARTMENT='RECEIVE_ADDDEPARTMENT'
export const CHANGE_DEPORDER = 'CHANGE_DEPORDER'

function requestDepartments() {
  return {
    type: REQUEST_DEPARTMENTS
  }
}

function receiveAddDepartment(name) {
  return {
    type: RECEIVE_ADDDEPARTMENT,
    name: name,
    receivedAt: Date.now()
  }
}

function receiveDepartments(json) {
  return {
    type: RECEIVE_DEPARTMENTS,
    departments: json.data.children.map(child => child.data),
    receivedAt: Date.now()
  }
}

export function fetchDepartments() {
  return dispatch => {
    dispatch(requestDepartments())
    dispatch(general.toggleFetching(true))
    return fetch(`http://www.reddit.com/r/${subreddit}.json`)
      .then(response => response.json())
      .then(dispatch(general.toggleFetching(false)))
      .then(json => dispatch(receiveDepartments(json)))
  }
}

export function addDepartment(name) {
  return dispatch => {
  dispatch(general.toggleFetching(true))
  dispatch(general.changeNewNameInDialog(''))
  //TODO: return response
  dispatch(general.toggleFetching(false))
  dispatch(receiveAddDepartment(name))
  return dispatch(general.toggleSnackBar('Отделение ' + name + ' было добавлено в список отделений'))
}
}

export function changeDepartmentsOrder(array) {
  return {
    type: CHANGE_DEPORDER,
    departments : array
  }
}
