/*
this file contains actions for blocks case
*/

import fetch from 'isomorphic-fetch'

import * as general from './index'

export const REQUEST_BLOCKS = 'REQUEST_BLOCKS'
export const RECEIVE_BLOCKS = 'RECEIVE_BLOCKS'
export const RECEIVE_ADDBLOCK='RECEIVE_ADDBLOCK'
export const CHANGE_BLOCKORDER = 'CHANGE_BLOCKORDER'

function requestBlocks() {
  return {
    type: REQUEST_BLOCKS
  }
}


function receiveAddBlock(name, isTemplate) {
  return {
    type: RECEIVE_ADDBLOCK,
    name: name,
    blockType: (isTemplate ? 'Doc creator' : 'Question list'),
    receivedAt: Date.now()
  }
}

function receiveBlocks(json) {
  return {
    type: RECEIVE_BLOCKS,
    departments: json.data.children.map(child => child.data),
    receivedAt: Date.now()
  }
}

export function fetchBlocks() {
  return dispatch => {
    dispatch(requestBlocks())
    dispatch(general.toggleFetching(true))
    return fetch(`http://www.reddit.com/r/${subreddit}.json`)
      .then(response => response.json())
      .then(dispatch(general.toggleFetching(true)))
      .then(json => dispatch(receiveBlocks(json)))
  }
}

export function addBlock(name, isTemplate) {
  return dispatch => {
  dispatch(general.toggleFetching(true))
  dispatch(general.changeNewNameInDialog(''))
  //TODO: return response
  dispatch(general.toggleFetching(false))
  dispatch(receiveAddBlock(name, isTemplate))
  return dispatch(general.toggleSnackBar('Блок ' + name + ' был добавлен в список блоков отделения.'))
}
}

export function changeBlocksOrder(array) {
  return {
    type: CHANGE_BLOCKORDER,
    blocks : array
  }
}
