/*
this file contains general actions for the whole application
*/

import fetch from 'isomorphic-fetch'
import * as listValueTypes from '../constants/listvaluetypes'

export const TOGGLE_SNACKBAR = 'TOGGLE_SNACKBAR'
export const CHANGE_PAGE = 'CHANGE_PAGE'
export const START_EDIT = 'START_EDIT'
export const EDIT_NAME = 'EDIT_NAME'
export const CHANGE_NEWNAME = 'CHANGE_NEWNAME'
export const TOGGLE_FETCHING = 'TOGGLE_FETCHING'
export const CHANGE_BLOCKTYPE= 'CHANGE_BLOCKTYPE'
export const EDIT_DEPNAME = "EDIT_DEPNAME"
export const EDIT_BLOCKNAME = "EDIT_BLOCKNAME"
export const CHANGE_DEPPAGE = "CHANGE_DEPPAGE"
export const CHANGE_BLOCKPAGE = "CHANGE_BLOCKPAGE"
export const CHANGE_TEMPLATEPAGE = "CHANGE_TEMPLATEPAGE"

export function toggleSnackBar(text) {
  return {
    type: TOGGLE_SNACKBAR,
    text : text
  }
}
export function toggleFetching(val) {
  return {
    type: TOGGLE_FETCHING,
    isFetching : val
  }
}
export function changeNewNameInDialog(name) {
  return {
    type: CHANGE_NEWNAME,
    name: name
  }
}
export function startEditing(id) {
  return {
    type: START_EDIT,
    id: id
  }
}

export function editName(name, id, type) {
  console.log(type, id);
  switch (type)
  {
    case "DEPARTMENT_LIST" :
    return {
      type : EDIT_DEPNAME,
      name : name,
      id : id
    }
    case "BLOCK_LIST":
    return {
      type : EDIT_BLOCKNAME,
      name : name,
      id : id
    }
  }
}

export function changePage(id, blockType, title, name, listtype) {
  if (id === -1) {
    return dispatch => {
    return dispatch(changeTitle(title+name, listtype, ''))
    }
  }
  return dispatch => {
  dispatch(changeTitle(title+name, listtype))
  return dispatch(changeContent(listtype, id, blockType))
  }
}

export function changeContent(listtype, id, blockType) {
  switch (listtype) {
    case listValueTypes.BLOCK_LIST:
    return {
      type : CHANGE_DEPPAGE,
      id : id
    }
    case listValueTypes.SUBBLOCK_LIST:
    if (blockType == listValueTypes.BLOCK_TYPE_1)
    return {
      type: CHANGE_BLOCKPAGE,
      id : id
    }
    else
    return {
      type: CHANGE_TEMPLATEPAGE,
      id : id
    }
  }
}

export function changeTitle(title, listType) {
  return {
    type: CHANGE_PAGE,
    name: title + name,
    listtype : listType
  }
}


export function changeBlockType(type) {
  return {
    type: CHANGE_BLOCKTYPE,
    blockType : type
  }
}
