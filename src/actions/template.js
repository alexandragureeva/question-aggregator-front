export const CHANGE_TEMPLATEORDER = 'CHANGE_TEMPLATEORDER'
export const ADD_TBLOCK='ADD_TBLOCK'
export const SET_NEWSTATE = 'SET_NEWSTATE'

export function changeTBlocksOrder(array) {
  return {
    type: CHANGE_TEMPLATEORDER,
    template : array
  }
}


export function addTBlock(tblock) {
  return {
    type: ADD_TBLOCK,
    text : tblock.text,
    hint : tblock.hint
  }
}

export function setNewState(id, state) {
  return {
    type: SET_NEWSTATE,
    id : id,
    state : state
  }
}
