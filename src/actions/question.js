/*
this file contains actions for questions case
*/

import fetch from 'isomorphic-fetch'

import * as general from './index'

export const REQUEST_QUESTIONS = 'REQUEST_QUESTIONS'
export const RECEIVE_QUESTIONS = 'RECEIVE_QUESTIONS'
export const RECEIVE_ADDQUESTION='RECEIVE_ADDQUESTION'
export const CHANGE_QUESTIONORDER = 'CHANGE_QUESTIONORDER'

function requestQuestions() {
  return {
    type: REQUEST_QUESTIONS
  }
}


function receiveAddQuestion(q) {
  return {
    type: RECEIVE_ADDQUESTION,
    q: q,
    receivedAt: Date.now()
  }
}

function receiveQuestions(json) {
  return {
    type: RECEIVE_QUESTIONS,
    departments: json.data.children.map(child => child.data),
    receivedAt: Date.now()
  }
}

export function fetchQuestions() {
  return dispatch => {
    dispatch(requestQuestions())
    dispatch(general.toggleFetching(true))
    return fetch(`http://www.reddit.com/r/${subreddit}.json`)
      .then(response => response.json())
      .then(dispatch(general.toggleFetching(true)))
      .then(json => dispatch(receiveQuestions(json)))
  }
}

export function addQuestion(q) {
  console.log(q);
  return dispatch => {
  dispatch(general.toggleFetching(true))
  //TODO: return response
  dispatch(general.changeNewNameInDialog(''))
  dispatch(general.toggleFetching(false))
  dispatch(receiveAddQuestion(q))
  return dispatch(general.toggleSnackBar('Вопрос ' + q.name + ' был добавлен в список вопросов.'))
}
}

export function changeQuestionsOrder(array) {
  return {
    type: CHANGE_QUESTIONORDER,
    questions : array
  }
}
