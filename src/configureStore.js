import { createStore, applyMiddleware, compose } from 'redux'
import thunkMiddleware from 'redux-thunk'
import createLogger from 'redux-logger'
import rootReducer, {INITIAL_STATE} from './reducers/index'

const loggerMiddleware = createLogger()
console.log(INITIAL_STATE)
export default function configureStore() {
  return createStore(
    rootReducer,
    INITIAL_STATE,
    compose(
      applyMiddleware(
        thunkMiddleware,
        loggerMiddleware
      ), window.devToolsExtension ? window.devToolsExtension() : f => f)

  )
}
