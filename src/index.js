import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {createStore, applyMiddleware} from 'redux';
import { Router, browserHistory } from 'react-router';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import injectTapEventPlugin from 'react-tap-event-plugin';
import configureStore from './configureStore'
import App from './containers/app'
// все наши action будут проходить через middleware, прежде чем достигнуть reducer
/*const createStoreWithMiddleware = applyMiddleware(
  promise
)( createStore );
*/
// подключаем роутер
injectTapEventPlugin();
const store = configureStore()
ReactDOM.render(
    <Provider store={store}>
    <MuiThemeProvider>
  <App/>
    </MuiThemeProvider>
  </Provider>
  , document.getElementById('root')
);



/*<Provider store={createStoreWithMiddleware(reducers)}>
    <Router history={ browserHistory } routes={ routes } />
  </Provider>*/
