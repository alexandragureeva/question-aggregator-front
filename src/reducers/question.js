import * as Const from '../actions/question';
import { CHANGE_BLOCKPAGE } from '../actions/index';
import * as additional from './functions'
const questions = {
    currentBlId : -1,
    currentQuestions : [],
    subBlocks : [{blockId : 234, ids : [0, 1, 2, 3, 4]}],
    qHeaders : [
      {
        id: 0,
        name: 'Возраст',
        uName : '0#dl',
        type: 'q',
        level: 0
      },
      {
        id: 1,
        name: 'Телефоны',
        uName : '1#ds',
        type: 'ql',
        level: 0
      },
      {
        id: 2,
        name: 'Мобильный телефон',
        uName : '2#dr',
        type: 'q',
        level: 1
      },
      {
        id: 3,
        name: 'Домашний телефон',
        uName : '3#dt',
        type: 'q',
        level: 1
      },
      {
        id: 4,
        name: 'Дополнительно',
        uName : '4#er',
        type: 'q',
        level: 0
      }
    ],
    qOptions : [
      {
        qid: 0,
        type: 'selone',
        answers : ['0-20', '21-40', '41-60', '61-80', '>80']
      },
      {
        qid: 2,
        type: 'text',
        answers : []
      },
      {
        qid: 3,
        type: 'text',
        answers : []
      },
      {
        qid: 4,
        type: 'text',
        answers : []
      }
    ]
}
export default function ( state = questions, action) {
  switch(action.type) {
    case Const.REQUEST_QUESTIONS:
    return Object.assign({}, state, {
     questions: []
   })
    case Const.RECEIVE_QUESTIONS:
    return Object.assign({}, state, {
     questions: action.questions
   })
   case Const.RECEIVE_ADDQUESTION:
   var id = Math.floor(Math.random()*1000);
   var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
   var uName = id + '#' + possible.charAt(Math.floor(Math.random() * possible.length)) +
   possible.charAt(Math.floor(Math.random() * possible.length));
   var newHeaders = additional.insertItem(state.qHeaders, {id: id, name : action.q.name, uName: uName, type: action.q.type, level: 0});
   var newOptions =  additional.insertItem(state.qOptions, {qid: id, type : action.q.opttype, answers: action.q.options});
   var newSubBlocks = additional.insertInQuestionArray(state.subBlocks,  state.currentBlId, id);
   var newCurrentBlocks = additional.getCurrentEntities(state.subBlocks, state.currentBlId);
   return Object.assign({}, state, {
     currentQuestions : newCurrentBlocks,
      qHeaders : newHeaders, qOptions : newOptions, subBlocks : newSubBlocks

  })
  case Const.CHANGE_QUESTIONORDER :
  if (action.questions.length > 0 && action.questions[0].level === 0) {
    var newSubBlocks = additional.changeEntitiesOrder(state.subBlocks, action.questions, state.currentBlId);
    return Object.assign({}, state, {
     subBlocks : newSubBlocks,
     currentQuestions : additional.getCurrentEntities(newSubBlocks, state.currentBlId)
    })
  } else {
    return Object.assign({}, state, {
     qHeaders : state.qHeaders//additional.replaceSubarray(state.qHeaders, action.questions)
    })
  }
  case CHANGE_BLOCKPAGE:
  return Object.assign({}, state, {
   currentBlId: action.id,
   currentQuestions : additional.getCurrentEntities(state.subBlocks, action.id)
 })
    default:
    return state
  }
}
