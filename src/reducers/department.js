import * as depConst from '../actions/department';
import { EDIT_DEPNAME } from '../actions/index'
import * as additional from './functions'
const departments = {
    departments : [{Name:"fms", Id: 345, Key: "skdfhsg"}]
}
export default function ( state = departments, action) {
  switch(action.type) {
    case depConst.REQUEST_DEPARTMENTS:
    return Object.assign({}, state, {
     departments: []
   })
    case depConst.RECEIVE_DEPARTMENTS:
    return Object.assign({}, state, {
     departments: action.departments
   })
   case depConst.RECEIVE_ADDDEPARTMENT:
   return Object.assign({}, state, {
    departments : additional.insertItem(state.departments, {Id:Math.floor(Math.random()*10000), Name: action.name, Key:"dkjh"})
  })
  case depConst.CHANGE_DEPORDER:
  return Object.assign({}, state, {
   departments : action.departments
 })
 case EDIT_DEPNAME:
 return Object.assign({}, state, {
  departments :  additional.replaceName(state.departments, action.id, action.name)
})
    default:
    return state
  }
}
