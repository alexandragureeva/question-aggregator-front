import * as Const from '../actions/template';
import { CHANGE_TEMPLATEPAGE } from '../actions/index';
import * as additional from './functions'
const templates = {
    currentTId : -1,
    currentTBlocks : [],
    subBlocks : [{blockId : 345, ids : [0]}],
    tblocks : [
      {
        id : 0,
        text : "some text",
        hint : "this is a hint"
      }
    ]
}
export default function ( state = templates, action) {
  switch(action.type) {
   case Const.ADD_TBLOCK:
   var id = Math.floor(Math.random()*1000);
   var newTBlocks = additional.insertItem(state.tblocks, {id: id, text : action.text, hint: action.hint});
   var newSubBlocks = additional.insertInQuestionArray(state.subBlocks,  state.currentTId, id);
   var newCurrentBlocks = additional.getCurrentEntities(state.subBlocks, state.currentTId);
   return Object.assign({}, state, {
     currentTBlocks : newCurrentBlocks,
     tblocks : newTBlocks,
     subBlocks : newSubBlocks
  })
  case CHANGE_TEMPLATEPAGE:
  return Object.assign({}, state, {
   currentTId: action.id,
   currentTBlocks : additional.getCurrentEntities(state.subBlocks, action.id)
 })
 case Const.SET_NEWSTATE:
 var newTBlocks = state.tblocks;
 for (var i = 0; i < newTBlocks.length; i++) {
   if (newTBlocks[i].id === action.id) {
     newTBlocks[i].text = action.state;
   }
 }
 return Object.assign({}, state, {
  tblocks : newTBlocks
})
case Const.CHANGE_TEMPLATEORDER:
var newSubBlocks = additional.changeEntitiesOrder(state.subBlocks, action.template, state.currentTId);
return Object.assign({}, state, {
 subBlocks : newSubBlocks,
 currentTBlocks : additional.getCurrentEntities(newSubBlocks, state.currentTId)
})
    default:
    return state
  }
}
