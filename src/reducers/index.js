import * as listValueTypes from '../constants/listvaluetypes'

import * as actions from '../actions/index'

import { combineReducers } from 'redux'
import departments from './department'
import blocks from './block'
import question from './question'
import template from './template'
import { reducer as formReducer } from 'redux-form';

 const general_initial_state = {
    isFetching : false,
    listType : listValueTypes.DEPARTMENT_LIST, //type of the list (dep list, block list, quest list) on the page
    blockType: '', //doc creator or questionairre
    name : listValueTypes.DEPARTMENT_NAME, //name of the main page component
    snackBarIsShowing : true,
    snackBarMessage : 'Выберите отделение или создайте новое.',
    editId : -1,
    editedName : '',
    newNameInDialog : ''
  }
export function general( state = general_initial_state, action) {
  switch(action.type) {
      case actions.TOGGLE_SNACKBAR:
        return Object.assign({}, state, {
        snackBarIsShowing : !state.snackBarIsShowing,
        snackBarMessage : action.text
       })
       case actions.TOGGLE_FETCHING:
       console.log(action.isFetching + 'kjshfgshfrj');
         return Object.assign({}, state, {
           isFetching : action.isFetching
        })
       case actions.CHANGE_PAGE:
         return Object.assign({}, state, {
         listType : action.listtype,
         name : action.name
      })
       case actions.START_EDIT:
        return Object.assign({}, state, {
        editId : action.id
     })
        case actions.START_EDIT:
           return Object.assign({}, state, {
           editedName : action.name
     })
        case actions.CHANGE_BLOCKTYPE:
           return Object.assign({}, state, {
           blockType : action.blockType
     })
     case actions.CHANGE_NEWNAME:
      return { ...state,    newNameInDialog : action.name }
    default: return state
  }
}


const rootReducer = combineReducers({
  departments : departments,
  blocks : blocks,
  general : general,
  question : question,
  template : template,
  formReducer
})

export default rootReducer
