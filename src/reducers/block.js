import * as Const from '../actions/block';
import { CHANGE_DEPPAGE, EDIT_BLOCKNAME } from '../actions/index'
import * as additional from './functions'
const blocks = {
    currentBlocks : [],
    currentDepId: -1,
    blocks : []
}
export default function ( state = blocks, action) {
  switch(action.type) {
    case Const.REQUEST_BLOCKS:
    return Object.assign({}, state, {
     currentBlocks: []
   })
    case Const.RECEIVE_BLOCKS:
    return Object.assign({}, state, {
     currentBlocks: action.blocks
   })
   case Const.RECEIVE_ADDBLOCK:
   var newBlocks = additional.insertInBlockArray(state.blocks, state.currentDepId, action.name, action.blockType);
   var newCurrentBlock = additional.getBlocks(newBlocks, state.currentDepId);
   return Object.assign({}, state, {
    blocks : newBlocks,
    currentBlocks : newCurrentBlock
  })
  case Const.CHANGE_BLOCKORDER :
  return Object.assign({}, state, {
   currentBlocks : action.blocks
 })
 case CHANGE_DEPPAGE:
 var newBlocks = additional.changeBlockArray(state.blocks, action.id);
 var curBlock = additional.getBlocks(newBlocks, action.id);
 return Object.assign({}, state, {
  currentBlocks : curBlock,
  blocks : newBlocks,
  currentDepId : action.id
})
case EDIT_BLOCKNAME:
var newBlocks = additional.replaceBlockName(state.blocks, state.currentDepId, action.id, action.name);
var curBlock = additional.getBlocks(newBlocks, state.currentDepId);
return Object.assign({}, state, {
 currentBlocks : curBlock,
 blocks : newBlocks
})
    default:
    return state
  }
}
