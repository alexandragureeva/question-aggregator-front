export function insertItem(array, val) {
    array =  [
        ...array.slice(0, val),
        val,
        ...array.slice(val)
    ]
    array.sort(function(a, b){
        if(a.Name < b.Name) return -1
        if(a.Name > b.Name) return 1
        return 0
    });
    return array
}

export function replaceName(array, id, name) {
    for (var i = 0; i < array.length; i++)
        if (array[i].Id === id) {
            array[i].Name = name;
            return array;
          }
    return array;
}

export function replaceBlockName(array, did, id, name) {
    for (var i = 0; i < array.length; i++)
        if (array[i].DepId === did) {
          for (var j = 0; j < array[i].Blocks.length; j++)
              if (array[i].Blocks[j].Id === id) {
                  array[i].Blocks[j].Name = name;
                  return array;
              }
          }
    return array;
}

export function replaceSubarray(mainArr, subArr) {
  var tempArr = []
  for (var i = 0; i < mainArr.length; i++) {
    if (mainArr[i] instanceof Array) {
      tempArr.push.apply(tempArr, mainArr[i])
    } else {
      tempArr.push(mainArr[i])
    }
  }
  var minIndex = tempArr.length -1
  for (var i = 0; i < subArr.length; i++) {
    var index = tempArr.findIndex(x => x.id==subArr[i].id)
    minIndex = (index <= minIndex) ? index : minIndex
  }
  Array.prototype.splice.apply(tempArr, [minIndex, subArr.length].concat(subArr))
  return tempArr
}


export function changeBlockArray(blockArray, id) {
  for (var i = 0; i < blockArray.length; i++)
      if (blockArray[i].DepId === id) {
          return blockArray;
        }
  var newBlock = {DepId: id, Blocks : []};
  blockArray.push(newBlock);
  return blockArray;
}

export function getBlocks(blockArray, id) {
  for (var i = 0; i < blockArray.length; i++)
      if (blockArray[i].DepId === id) {
          return blockArray[i].Blocks;
        }
  return null;
}

export function insertInBlockArray(blockArray, id, name, type) {
  for (var i = 0; i < blockArray.length; i++)
      if (blockArray[i].DepId === id) {
          blockArray[i].Blocks = insertItem(blockArray[i].Blocks, {Name: name, Id: Math.floor(Math.random()*10000), Type: type, Number: blockArray[i].Blocks.length})
          return blockArray;
        }
  return blockArray;
}

export function insertInQuestionArray(qArray, blId, id) {
  for (var i = 0; i < qArray.length; i++)
      if (qArray[i].blockId === blId) {
          qArray[i].ids.push(id);
          return qArray;
        }
  return qArray;
}


export function getCurrentEntities(array, id) {
  for (var i = 0; i < array.length; i++)
      if (array[i].blockId === id) {
          return array[i].ids;
        }
  var newBlock = {blockId : id, ids : []};
  array.push(newBlock);
  return [];
}


export function changeEntitiesOrder(subBlocks, orderArray, id) {
  var orderedIds = [];
    for (var i = 0; i < orderArray.length; i++) {
      orderedIds.push(orderArray[i].id);
    }
    for (var i = 0; i < subBlocks.length; i++)
        if (subBlocks[i].blockId === id) {
            subBlocks[i].ids = orderedIds;
            return subBlocks;
          }
    return subBlocks;
}


export function getAllQuestions(questionnaires) {
  var result = [];
  for (var i = 0; i < questionnaires.length; i++) {
    result.push({name: questionnaires[i].name, uName: questionnaires[i].uName})
  }
  return result;
}
