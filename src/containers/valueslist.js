import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'

import { fetchDepartments, changeDepartmentsOrder} from '../actions/department'
import { fetchBlocks, changeBlocksOrder} from '../actions/block'
import {changeQuestionsOrder} from '../actions/question'
import { changeTBlocksOrder, setNewState } from '../actions/template'
import * as generalActions from '../actions/index'
import * as functions from '../reducers/functions'

import Block from '../components/block'
import QuestionList from '../components/questionList/questionList'
import TemplateList from '../components/templateList/templateList'


import * as listValueTypes from '../constants/listvaluetypes'

import {List} from 'material-ui/List'
import Paper from 'material-ui/Paper'


import {SortableContainer, SortableElement, SortableHandle, arrayMove} from 'react-sortable-hoc';


class ValuesList extends Component {


  constructor(props) {
    super(props)
    this.handleOnDepartmentClick = this.handleOnDepartmentClick.bind(this)
    this.handleOnBlockClick = this.handleOnBlockClick.bind(this)
  }

  componentDidMount() {
    switch (this.props.listtype) {
      case listValueTypes.DEPARTMENT_LIST :
      // this.props.fetchDepartments()
       break
      case listValueTypes.BLOCK_LIST :
    //    this.props.fetchBlocks()
        break
      case listValueTypes.SUBBLOCK_LIST :
       break
    }
  }

  handleOnDepartmentClick(id, name) {
    //action to get the list of blocks with id
      this.props.changePage(id, '', listValueTypes.BLOCK_NAME, name, listValueTypes.BLOCK_LIST)
  }


  handleOnBlockClick(id, name, type) {
    //fill
    console.log('type: ' + type);
    this.props.changeBlockType(type)
    this.props.changePage(id, type,  '', name, listValueTypes.SUBBLOCK_LIST)
  }

  handleOnEditClick(id) {
    if (this.props.editId === -1) {
      this.props.startEditing(id)
    } else {
      this.props.startEditing(-1)
    }
  }

  setOptions(headers) {
    var q = this.props.questions;
    var options = [];
    if (q.currentBlId !== -1) {
      for (var i = 0; i < headers.length; i++) {
        for (var j = 0; j < q.qOptions.length; j++) {
            if (headers[i].id === q.qOptions[j].qid) {
              options.push(q.qOptions[j]);
            }
        }
      }
      return options;
    } else {
      return options;
    }
  }

  setHeaders() {
    var q = this.props.questions;
    var headers = [];
    if (q.currentBlId !== -1) {
      for (var i = 0; i < q.currentQuestions.length; i++) {
        for (var j = 0; j < q.qHeaders.length; j++) {
          if (q.qHeaders[j].id === q.currentQuestions[i]) {
            headers.push(q.qHeaders[j]);
          }
        }
      }
      return headers;
    } else {
      return headers;
    }
  }

  setTBlocks(ids, allTBlocks) {
    var tBlocks = [];
    for (var i = 0; i < ids.length; i++) {
      for (var j = 0; j < allTBlocks.length; j++) {
        if (allTBlocks[j].id === ids[i]) {
          tBlocks.push(allTBlocks[j]);
        }
      }
    }
    return tBlocks;
  }

  render() {
    var DragHandle = '';
    var headers = this.setHeaders();
    var options = this.setOptions(headers);
    if (this.props.listtype === listValueTypes.SUBBLOCK_LIST) {
        if (this.props.blockType === listValueTypes.BLOCK_TYPE_1) {
          return <QuestionList questions={headers}
                              options ={options}
                              changeQuestionsOrder={this.props.changeQuestionsOrder}
                              level = {0}/>
        } else {
          var questionnaires = functions.getAllQuestions(this.props.questions.qHeaders);
          var tBlocksIds = functions.getCurrentEntities(
            this.props.templates.subBlocks, this.props.templates.currentTId);
          var tBlocks = this.setTBlocks(tBlocksIds, this.props.templates.tblocks);
          return <TemplateList questionnaires={questionnaires}
                              tBlocks ={tBlocks}
                              changeTBlocksOrder={this.props.changeTBlocksOrder}
                              setNewState = {this.props.setNewState}/>
        }
    } else {
    var docType = (this.props.listtype === listValueTypes.BLOCK_LIST) ?
        (this.props.blockType === listValueTypes.BLOCK_TYPE_1) ?
        'Questionnaire: ' : 'Template: '  :''
    DragHandle = SortableHandle((e) =>
    {
      return (
        <Block Key={this.props.listtype === listValueTypes.DEPARTMENT_LIST ?  e.value.Key : 'null'} Name={e.value.Name}
     Id={e.value.Id}
     onClick={() => (this.props.listtype === listValueTypes.DEPARTMENT_LIST) ?
        methodToProcessWhenClick(e.value.Id, e.value.Name) :
        methodToProcessWhenClick(e.value.Id, e.value.Name, e.value.Type)}
     onEditClick={() => this.handleOnEditClick(e.value.Id)} edit = {this.props.editId === e.value.Id}
      onChange = {this.props.editName}/>
    );
  });

    var SortableItem = SortableElement(({value}) => {
      return (
          <li>
              <DragHandle value={value}/>
          </li>
      )
  });
    var SortableList = SortableContainer(({items}) => {
    	return (
    		<ul style={{listStyleType: 'none'}}>
    			{items.map((value, index) =>
                    <SortableItem key={`item-${index}`} index={index} value={value} />
                )}
    		</ul>
    	);
    });
    var onSortEnd = '';
    var methodToProcessWhenClick = null
    var arrayToProcess = []
    switch(this.props.listtype) {
      case listValueTypes.DEPARTMENT_LIST :
       methodToProcessWhenClick = this.handleOnDepartmentClick;
       arrayToProcess = this.props.departments;
       onSortEnd = ({oldIndex, newIndex}) => { this.props.changeDepartmentsOrder(arrayMove(arrayToProcess, oldIndex, newIndex))}
       break
      case listValueTypes.BLOCK_LIST :
      methodToProcessWhenClick = this.handleOnBlockClick;
      arrayToProcess = this.props.blocks //here
      onSortEnd = ({oldIndex, newIndex}) => { this.props.changeBlocksOrder(arrayMove(arrayToProcess, oldIndex, newIndex))}
      break
      default :
      break
    }
    return (
      <SortableList items={arrayToProcess} onSortEnd={onSortEnd} useDragHandle={true} />
    )
  }
}
}
ValuesList.propTypes = {
  listtype : PropTypes.string.isRequired,
  departments : PropTypes.arrayOf(PropTypes.shape({
    Id: PropTypes.number.isRequired,
    Name: PropTypes.string.isRequired,
    Key: PropTypes.string.isRequired
  }).isRequired).isRequired,
  blocks : PropTypes.arrayOf(PropTypes.shape({
    Id: PropTypes.number.isRequired,
    Name: PropTypes.string.isRequired,
    Type: PropTypes.string.isRequired,
    Number: PropTypes.number.isRequired
  }).isRequired).isRequired,
  editId : PropTypes.number.isRequired
}

function mapStateToProps(state) {
  return {
  listtype : state.general.listType,
  blockType : state.general.blockType,
  departments : state.departments.departments,
  blocks : state.blocks.currentBlocks,
  questions : state.question,
  templates : state.template,
  editId: state.general.editId
}
}


function mapDispatchToProps(dispatch) {
    return({
        changePage: (id, blockType, title, name, listtype) =>
        {dispatch(generalActions.changePage(id, blockType, title, name, listtype))},
        startEditing : (id) => {dispatch(generalActions.startEditing(id))},
        editName : (name, id, type) => {dispatch(generalActions.editName(name, id, type))},
        changeDepartmentsOrder: (arr) =>{dispatch(changeDepartmentsOrder(arr))},
        changeBlocksOrder: (arr) =>{dispatch(changeBlocksOrder(arr))},
        changeQuestionsOrder : (arr)=>{dispatch(changeQuestionsOrder(arr))},
        changeTBlocksOrder : (arr) => {dispatch(changeTBlocksOrder(arr))},
        setNewState: (id, state) => {dispatch(setNewState(id, state))},
        changeBlockType : (type)=>{dispatch(generalActions.changeBlockType(type))}
    })
}
export default connect(mapStateToProps, mapDispatchToProps)(ValuesList)
