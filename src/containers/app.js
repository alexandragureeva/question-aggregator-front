import React, {Component} from 'react'
import { connect, Provider } from 'react-redux'

import MainPage from '../components/mainpage'

import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import RefreshIndicator from 'material-ui/RefreshIndicator';
 import { Flex, Box } from 'reflexbox'
import AppBar from 'material-ui/AppBar'
import Snackbar from 'material-ui/Snackbar'
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';

import * as listValueTypes from '../constants/listvaluetypes'

import {addDepartment} from '../actions/department'
import {addBlock} from '../actions/block'
import {addQuestion} from '../actions/question'
import { addTBlock } from '../actions/template'
import {toggleSnackBar, changePage} from '../actions/index'

export class App extends Component {
  render() {
    var methodToProcess =  this.defineOnClick();
    var labelForDialog = '';
    var titleForDialog = '';
    switch (this.props.listType) {
        case listValueTypes.DEPARTMENT_LIST :
          labelForDialog = 'Новое отделение'
          titleForDialog = 'Добавить новое отделение'
          break
        case listValueTypes.BLOCK_LIST :
          labelForDialog = 'Новый блок информации'
          titleForDialog = 'Добавить новый блок информации'
          break
        case listValueTypes.SUBBLOCK_LIST :
          if (this.props.blockType === listValueTypes.BLOCK_TYPE_1) {
            labelForDialog = 'Новый вопрос/список вопросов'
            titleForDialog = 'Добавить новый вопрос/список вопросов'
          } else {
            labelForDialog = 'Новый блок документа'
            titleForDialog = 'Добавить новый блок документа'
          }
          break
      }
    return (
      <div style={{position: 'relative'}}>
        <AppBar
        title="Medical Record Generation System"
        iconElementLeft = {
            <IconMenu
             iconButtonElement={<IconButton iconStyle={{ color:'white'}}>
             <MoreVertIcon/></IconButton>}
             anchorOrigin={{horizontal: 'left', vertical: 'top'}}
             targetOrigin={{horizontal: 'left', vertical: 'top'}}
           >
             <MenuItem primaryText="Отделения" onClick = {
               ()=>{  this.props.changePage(-1, '', listValueTypes.DEPARTMENT_NAME, '', listValueTypes.DEPARTMENT_LIST)}}/>
             <MenuItem primaryText="Блоки" />
           </IconMenu>
        }
      />
      <MainPage name = { this.props.name } title={titleForDialog}
            label = {labelForDialog} onAddClick = { methodToProcess }
            nameForDialog = {this.props.newNameInDialog} type={this.props.listType}
            blockType = {this.props.blockType} />
      <Snackbar
            open={this.props.snackBarIsShowing}
            message= {this.props.snackBarMessage}
            autoHideDuration={1000}
            onRequestClose={() => this.props.toggleSnackBar('')}/>
      <RefreshIndicator
           size={50}
        left={-25}
        top={-25}
        style={{marginLeft: '50%', marginTop: '100%'}}
           status={this.props.isFetching ? 'loading' : 'hide'}
         />
      </div>
    )
  }

  defineOnClick () {
    switch (this.props.listType) {
        case listValueTypes.DEPARTMENT_LIST : return this.onAddNewDepartmentClick
        case listValueTypes.BLOCK_LIST : return this.onAddNewBlockClick
        case listValueTypes.SUBBLOCK_LIST :
          if (this.props.blockType === listValueTypes.BLOCK_TYPE_1)
            return this.onAddNewQuestionClick
            else
            return this.OnAddNewTBlockClick
    }
  }

  //TODO: fill functions
  onAddNewDepartmentClick = name => {
    this.props.addDepartment(name)
  }

  onAddNewBlockClick = (name, isTemplate) => {
    //here
    this.props.addBlock(name, isTemplate)
  }

  onAddNewQuestionClick = q => {
    this.props.addQuestion(q)
  }

  OnAddNewTBlockClick = (text, hint) => {
  this.props.addTBlock({text : text, hint : hint});
}
}



  function mapStateToProps(state) {
    return {
        listType: state.general.listType,
        blockType : state.general.blockType,
        name : state.general.name,
        newNameInDialog: state.general.newNameInDialog,
        snackBarIsShowing: state.general.snackBarIsShowing,
        snackBarMessage: state.general.snackBarMessage,
        isFetching: state.general.isFetching
    }
  }

export default connect(mapStateToProps, {addDepartment, toggleSnackBar, addBlock, addQuestion, addTBlock, changePage})(App);
