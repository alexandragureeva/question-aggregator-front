var path = require('path')
var webpack = require('webpack')
module.exports = {
    devtool: 'cheap-module-eval-source-map',
    devServer: {
        historyApiFallback: true
    },
    entry: [
'webpack-hot-middleware/client',
'babel-polyfill',
'./src/index'
],
    output: {
        path: path.join(__dirname, 'dist'),
        filename: 'bundle.js',
        publicPath: '/static/'
    },
    plugins: [
new webpack.optimize.OccurenceOrderPlugin(),
new webpack.HotModuleReplacementPlugin(),
new webpack.NoErrorsPlugin(),
new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery"
        })
],
    module: {
        loaders: [
            {
                loaders: ['babel-loader'],
                include: [
path.resolve(__dirname, "src"),
],
                test: /\.js$/,
                plugins: ['transform-runtime'],
},
{ test: /\.css$/, loader: 'style!css' },
            {
                test: /vendor\/.+\.(jsx|js)$/,
                loader: 'imports?jQuery=jquery,$=jquery,this=>window'
			},
      {
        test: /\.css$/,
        loader: 'style!css?modules',
        include: /flexboxgrid/,
      }
]
    }
}
